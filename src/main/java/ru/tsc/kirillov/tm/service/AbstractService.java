package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.repository.IRepository;
import ru.tsc.kirillov.tm.api.service.IService;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) findAll();
        return repository.findAll(comparator);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= repository.count())
            throw new IndexOutOfBoundsException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);

    }

    @Override
    public M removeById(final String id) {
        if (id == null || id.isEmpty())
            throw new IdEmptyException();
        M model = repository.removeById(id);
        if (model == null)
            throw new ProjectNotFoundException();
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) {
        if (index == null || index < 0 || index >= repository.count())
            throw new IndexOutOfBoundsException();
        return repository.removeByIndex(index);
    }

    @Override
    public Integer count() {
        return repository.count();
    }

}

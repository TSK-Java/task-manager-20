package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.service.IAuthService;
import ru.tsc.kirillov.tm.api.service.IUserService;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.exception.field.LoginEmptyException;
import ru.tsc.kirillov.tm.exception.field.PasswordEmptyException;
import ru.tsc.kirillov.tm.exception.system.AccessDeniedException;
import ru.tsc.kirillov.tm.exception.system.PermissionException;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public User getUser() {
        return userService.findOneById(getUserId());
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}

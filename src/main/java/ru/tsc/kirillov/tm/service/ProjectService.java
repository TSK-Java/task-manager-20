package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.model.Project;

import java.util.Date;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw  new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw  new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId, final String name, final String description, final Date dateBegin,
                          final Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw  new UserIdEmptyException();
        final Project project = create(userId, name, description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw  new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw  new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.count(userId)) throw new IndexOutOfBoundsException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw  new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw  new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.count(userId)) throw new IndexOutOfBoundsException();
        if (status == null) throw new StatusEmptyException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        return project;
    }

}

package ru.tsc.kirillov.tm.enumerated;

public enum Role {

    USUAL("Обычный пользователь"),
    ADMIN("Администратор");

    private String displayName;

    Role(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}

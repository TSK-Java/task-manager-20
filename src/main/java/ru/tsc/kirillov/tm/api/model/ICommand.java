package ru.tsc.kirillov.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}

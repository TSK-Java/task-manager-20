package ru.tsc.kirillov.tm.api.repository;

import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    void clear();

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    void removeAll(Collection<M> collection);

    M removeById(String id);

    M removeByIndex(Integer index);

    Integer count();

}

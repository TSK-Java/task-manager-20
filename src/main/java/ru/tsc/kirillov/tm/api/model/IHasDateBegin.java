package ru.tsc.kirillov.tm.api.model;

import java.util.Date;

public interface IHasDateBegin {

    Date getDateBegin();

    void setDateBegin(Date dateBegin);

}

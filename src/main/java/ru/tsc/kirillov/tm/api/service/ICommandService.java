package ru.tsc.kirillov.tm.api.service;

import ru.tsc.kirillov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getCommands();

    void add(AbstractCommand command);

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArgument(String argument);

}

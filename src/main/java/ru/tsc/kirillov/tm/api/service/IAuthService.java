package ru.tsc.kirillov.tm.api.service;

import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

    boolean isAuth();

    User getUser();

    String getUserId();

    void checkRoles(Role[] roles);

}

package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskCompletedbyIndexCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-completed-by-index";
    }

    @Override
    public String getDescription() {
        return "Завершить задачу по её индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Завершение задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        getTaskService().changeTaskStatusByIndex(getUserId(), NumberUtil.fixIndex(index), Status.COMPLETED);
    }

}

package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-change-status-by-index";
    }

    @Override
    public String getDescription() {
        return "Изменить статус проекта по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Изменение статуса проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите статус:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusByIndex(getUserId(), NumberUtil.fixIndex(index), status);
    }

}

package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @Override
    public String getDescription() {
        return "Изменить статус задачи по её индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Изменение статуса задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        System.out.println("Введите статус:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusByIndex(getUserId(), NumberUtil.fixIndex(index), status);
    }

}

package ru.tsc.kirillov.tm.command.user;

import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "Выполнение авторизации.";
    }

    @Override
    public void execute() {
        System.out.println("[Авторизация]");
        System.out.println("Введите логин");
        final String login = TerminalUtil.nextLine();
        System.out.println("Введите пароль");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}

package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public String getDescription() {
        return "Удалить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Удаление проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = getProjectService().findOneByIndex(getUserId(), NumberUtil.fixIndex(index));
        if (project == null)
            throw new ProjectNotFoundException();
        getProjectTaskService().removeProjectById(getUserId(), project.getId());
    }

}

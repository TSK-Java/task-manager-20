package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-update-by-id";
    }

    @Override
    public String getDescription() {
        return "Обновить задачу по её ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Обновление задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        getTaskService().updateById(getUserId(), id, name, description);
    }

}

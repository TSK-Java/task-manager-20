package ru.tsc.kirillov.tm.command.system;

import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationAllArgumentCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public String getArgument() {
        return "-arg";
    }

    @Override
    public String getDescription() {
        return "Отображение списка аргументов.";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand cmd: commands) {
            final String argument = cmd.getArgument();
            if (argument != null && !argument.isEmpty())
                System.out.println(cmd.getArgument());
        }
    }

}

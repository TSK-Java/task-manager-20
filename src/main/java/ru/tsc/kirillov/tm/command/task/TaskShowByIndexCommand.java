package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskShowCommand {

    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Отобразить задачу по её индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение задачи по индексу]");
        System.out.println("Введите индекс задачи:");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = getTaskService().findOneByIndex(getUserId(), NumberUtil.fixIndex(index));
        showTask(task);
    }

}

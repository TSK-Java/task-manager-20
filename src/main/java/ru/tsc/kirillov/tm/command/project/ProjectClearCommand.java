package ru.tsc.kirillov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Удалить все проекты.";
    }

    @Override
    public void execute() {
        System.out.println("[Очистка списка проектов]");
        getProjectTaskService().clear(getUserId());
    }

}

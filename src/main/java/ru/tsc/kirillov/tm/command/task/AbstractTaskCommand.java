package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

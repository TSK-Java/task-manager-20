package ru.tsc.kirillov.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Удалить все задачи.";
    }

    @Override
    public void execute() {
        System.out.println("[Очистка списка задач]");
        getTaskService().clear(getUserId());
    }

}

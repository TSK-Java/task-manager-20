package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectShowCommand {

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Отобразить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        final Project project = getProjectService().findOneByIndex(getUserId(), NumberUtil.fixIndex(index));
        showProject(project);
    }

}

package ru.tsc.kirillov.tm.command.system;

import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationAllCommandCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "commands";
    }

    @Override
    public String getArgument() {
        return "-cmd";
    }

    @Override
    public String getDescription() {
        return "Отображение списка команд.";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand cmd: commands)
            System.out.println(cmd.getName());
    }

}

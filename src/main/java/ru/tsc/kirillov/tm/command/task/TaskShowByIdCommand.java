package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskShowCommand {

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Отобразить задачу по её ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(getUserId(), id);
        showTask(task);
    }

}

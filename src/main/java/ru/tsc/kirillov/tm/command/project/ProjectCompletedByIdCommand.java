package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectCompletedByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-completed-by-id";
    }

    @Override
    public String getDescription() {
        return "Завершить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("Завершение проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(getUserId(), id, Status.COMPLETED);
    }

}

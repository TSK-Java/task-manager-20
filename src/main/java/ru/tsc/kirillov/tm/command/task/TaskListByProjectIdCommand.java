package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskListByProjectIdCommand extends AbstractTaskListCommand {

    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public String getDescription() {
        return "Отобразить список задач проекта.";
    }

    @Override
    public void execute() {
        System.out.println("[Список задач проекта]");
        System.out.println("Введите ID проекта:");
        final String projectId = TerminalUtil.nextLine();
        printTask(getTaskService().findAllByProjectId(getUserId(), projectId));
    }

}

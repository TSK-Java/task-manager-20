package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectUnbindTaskByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-unbind-task-by-id";
    }

    @Override
    public String getDescription() {
        return "Отвязать задачу от проекта.";
    }

    @Override
    public void execute() {
        System.out.println("[Отвязка задачи от проекта]");
        System.out.println("Введите ID проекта:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Введите ID задачи:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskToProject(getUserId(), projectId, taskId);
    }

}

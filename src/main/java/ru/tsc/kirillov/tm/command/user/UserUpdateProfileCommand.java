package ru.tsc.kirillov.tm.command.user;

import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "update-profile";
    }

    @Override
    public String getDescription() {
        return "Изменение данных профиля пользователя.";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[Изменение данных профиля пользователя]");
        System.out.println("Введите имя");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Введите фамилию");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Введите отчество");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

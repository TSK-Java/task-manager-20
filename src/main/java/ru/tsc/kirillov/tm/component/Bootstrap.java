package ru.tsc.kirillov.tm.component;

import ru.tsc.kirillov.tm.api.repository.ICommandRepository;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.api.service.*;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.command.project.*;
import ru.tsc.kirillov.tm.command.system.*;
import ru.tsc.kirillov.tm.command.task.*;
import ru.tsc.kirillov.tm.command.user.*;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.kirillov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.repository.CommandRepository;
import ru.tsc.kirillov.tm.repository.ProjectRepository;
import ru.tsc.kirillov.tm.repository.TaskRepository;
import ru.tsc.kirillov.tm.repository.UserRepository;
import ru.tsc.kirillov.tm.service.*;
import ru.tsc.kirillov.tm.util.DateUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService =  new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationAllCommandCommand());
        registry(new ApplicationAllArgumentCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskCompletedbyIdCommand());
        registry(new TaskCompletedbyIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectCompletedByIdCommand());
        registry(new ProjectCompletedByIndexCommand());
        registry(new ProjectBindTaskByIdCommand());
        registry(new ProjectUnbindTaskByIdCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** Добро пожаловать в Task Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** Task Manager завершил свою работу **");
            }
        });
    }

    private void initData() {
        taskService.create("Тестовая задача", "Простая задача");
        taskService.create("Вторая задача", "Простая задача");
        taskService.create("Ещё одна задача", "Простая задача");

        projectService.create("Тестовый проект", "Простой проект");
        projectService.create("Второй проект", "Простой проект");
        projectService.create("Ещё один проект", "Простой проект");
        projectService.add(
                new Project(
                        "Тест с датой",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("04.10.2019")
                )
        );
        projectService.add(
                new Project(
                        "Не запущенный тестовый проект",
                        Status.NOT_STARTED,
                        DateUtil.toDate("05.03.2018")
                )
        );
        projectService.add(
                new Project(
                        "Выполняющийся проект",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("16.02.2020")
                )
        );
        projectService.add(
                new Project(
                        "Завершённый проект",
                        Status.COMPLETED,
                        DateUtil.toDate("22.01.2021")
                )
        );
    }

    private boolean processArgument(final String[] args) {
        if (args == null || args.length == 0)
            return false;

        final String firstArg = args[0];
        processArgument(firstArg);

        return true;
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null)
            throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void close() {
        System.exit(0);
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) {
        if (processArgument(args))
            close();

        initData();
        initLogger();
        initUsers();
        while (true) {
            try {
                System.out.println("\nВведите команду:");
                final String cmdText = TerminalUtil.nextLine();
                processCommand(cmdText);
                System.out.println("[Ок]");
                loggerService.command(cmdText);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[Ошибка]");
            }
        }
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}

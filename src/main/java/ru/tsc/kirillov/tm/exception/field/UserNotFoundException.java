package ru.tsc.kirillov.tm.exception.field;

public final class UserNotFoundException extends AbstractFieldException {

    public UserNotFoundException() {
        super("Ошибка! Пользователь не найден.");
    }

}

package ru.tsc.kirillov.tm.exception.system;

import ru.tsc.kirillov.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Ошибка! Команда не поддерживается.");
    }

    public CommandNotSupportedException(final String cmdName) {
        super("Ошибка! Команда `" + cmdName + "` не поддерживается.");
    }

}

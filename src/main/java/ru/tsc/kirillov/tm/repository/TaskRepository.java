package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }

    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: models) {
            if (task.getProjectId() != null
                    && task.getUserId().equals(userId)
                    && task.getProjectId().equals(projectId))
                result.add(task);
        }
        return result;
    }

}

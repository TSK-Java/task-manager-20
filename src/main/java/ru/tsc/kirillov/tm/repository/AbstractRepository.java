package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.repository.IRepository;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public M findOneById(final String id) {
        for (final M model: models) {
            if (id.equals(model.getId()))
                return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.removeAll(collection);
    }

    @Override
    public M removeById(final String id) {
        final M model = findOneById(id);
        if (model == null)
            return null;

        models.remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null)
            return null;

        models.remove(model);
        return model;
    }

    @Override
    public Integer count() {
        return models.size();
    }

}
